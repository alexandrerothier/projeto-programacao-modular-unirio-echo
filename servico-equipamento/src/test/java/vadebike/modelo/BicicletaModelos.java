package vadebike.modelo;

import vadebike.dominio.Bicicleta;
import vadebike.dominio.BicicletaStatus;
import vadebike.dominio.dto.BicicletaNaRede;
import vadebike.dominio.dto.PostBicicleta;

public class BicicletaModelos {

    // TODO Integrar outros testes com a inicialização de dados no banco

    public static Bicicleta getBicicleta() {
        Bicicleta bicicleta = new Bicicleta();
        bicicleta.setId("123");
        bicicleta.setMarca("123");
        bicicleta.setModelo("123");
        bicicleta.setAno("123");
        bicicleta.setNumero(123);
        bicicleta.setStatus(BicicletaStatus.DISPONIVEL);
        return bicicleta;
    }

    public static PostBicicleta getPostBicicleta() {
        PostBicicleta postBicicleta = new PostBicicleta();
        postBicicleta.setStatus(BicicletaStatus.DISPONIVEL);
        postBicicleta.setMarca("123");
        postBicicleta.setModelo("123");
        postBicicleta.setAno("123");
        postBicicleta.setNumero(123);
        return postBicicleta;
    }

    public static BicicletaNaRede getBicicletaNaRede() {
        BicicletaNaRede bicicletaNaRede = new BicicletaNaRede();
        bicicletaNaRede.setIdBicicleta("123");
        bicicletaNaRede.setIdTranca("123");
        return bicicletaNaRede;
    }
}

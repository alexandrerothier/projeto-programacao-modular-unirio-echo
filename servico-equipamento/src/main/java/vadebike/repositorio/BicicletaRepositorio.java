package vadebike.repositorio;

import vadebike.banco.BancoDeDados;
import vadebike.dominio.Bicicleta;
import vadebike.dominio.BicicletaStatus;
import vadebike.dominio.Tranca;
import vadebike.dominio.TrancaStatus;
import vadebike.dominio.dto.BicicletaNaRede;
import vadebike.dominio.dto.PostBicicleta;
import vadebike.excecoes.GenericApiException;
import vadebike.util.Validador;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class BicicletaRepositorio {
    public Bicicleta salvarBicicleta(PostBicicleta postBicicleta) {
        Validador.validarCampos(postBicicleta);
        Bicicleta bicicleta = new Bicicleta();
        bicicleta.setId(UUID.randomUUID().toString());
        bicicleta.setStatus(postBicicleta.getStatus());
        bicicleta.setModelo(postBicicleta.getModelo());
        bicicleta.setAno(postBicicleta.getAno());
        bicicleta.setNumero(postBicicleta.getNumero());
        bicicleta.setMarca(postBicicleta.getMarca());
        BancoDeDados.BICICLETA_TABLE.put(bicicleta.getId(), bicicleta);
        return bicicleta;
    }

    public List<Bicicleta> buscarBicicletas() {
        return BancoDeDados.BICICLETA_TABLE.keySet()
                .stream()
                .map(BancoDeDados.BICICLETA_TABLE::get)
                .collect(Collectors.toList());
    }

    public Bicicleta buscarBicicleta(String id) {
        Bicicleta bicicleta = BancoDeDados.BICICLETA_TABLE.get(id);
        if (bicicleta == null) throw new GenericApiException(404, "Bicicleta não encontrado");
        return bicicleta;
    }

    public Bicicleta atualizarBicicleta(String id, Bicicleta novaBicicleta){
        Validador.validarCampos(novaBicicleta);
        buscarBicicleta(id);
        BancoDeDados.BICICLETA_TABLE.replace(id, novaBicicleta);
        return novaBicicleta;
    }

    public void deletarBicicleta(String id){
        Bicicleta bicicleta = buscarBicicleta(id);

        if (bicicleta.getStatus() != BicicletaStatus.DISPONIVEL) BancoDeDados.BICICLETA_TABLE.remove(id);
        else throw new GenericApiException(422, "Não é possível deletar uma bicicleta na rede");
    }

    public Bicicleta alterarStatus(String id, String status){
        Bicicleta bicicleta = buscarBicicleta(id);
        bicicleta.setStatus(BicicletaStatus.valueOf(status));
        BancoDeDados.BICICLETA_TABLE.replace(id, bicicleta);
        return bicicleta;
    }

    public void integrarNaRede(BicicletaNaRede bicicletaNaRede) {
        Validador.validarCampos(bicicletaNaRede);
        Bicicleta bicicleta = buscarBicicleta(bicicletaNaRede.getIdBicicleta());
        Tranca tranca = buscarTranca(bicicletaNaRede.getIdTranca());
        if (tranca.getIdBicicleta() != null) throw new GenericApiException(422, "Tranca ocupada.");
        tranca.setIdBicicleta(bicicleta.getId());
//        tranca.setStatus(TrancaStatus.OCUPADA);
//        bicicleta.setStatus(BicicletaStatus.DISPONIVEL);
        BancoDeDados.BICICLETA_TABLE.replace(bicicleta.getId(), bicicleta);
        BancoDeDados.TRANCA_TABLE.replace(tranca.getId(), tranca);
    }

    public void removerDaRede(BicicletaNaRede bicicletaNaRede) {
        Validador.validarCampos(bicicletaNaRede);
        Bicicleta bicicleta = buscarBicicleta(bicicletaNaRede.getIdBicicleta());
        Tranca tranca = buscarTranca(bicicletaNaRede.getIdTranca());
        if (tranca.getIdBicicleta() == null) throw new GenericApiException(422, "Tranca sem bicicleta.");
        tranca.setIdBicicleta(null);
        BancoDeDados.BICICLETA_TABLE.replace(bicicleta.getId(), bicicleta);
        BancoDeDados.TRANCA_TABLE.replace(tranca.getId(), tranca);
    }

    private Tranca buscarTranca(String id) {
        return Optional.of(BancoDeDados.TRANCA_TABLE.get(id))
                .orElseThrow(() -> new GenericApiException(404, "Tranca não encontrado."));
    }
}

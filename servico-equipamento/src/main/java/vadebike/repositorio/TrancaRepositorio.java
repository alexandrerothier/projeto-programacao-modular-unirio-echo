package vadebike.repositorio;

import vadebike.banco.BancoDeDados;
import vadebike.dominio.Bicicleta;
import vadebike.dominio.Totem;
import vadebike.dominio.Tranca;
import vadebike.dominio.TrancaStatus;
import vadebike.dominio.dto.PostTranca;
import vadebike.dominio.dto.TrancaNaRede;
import vadebike.excecoes.GenericApiException;
import vadebike.util.Validador;

import java.util.*;
import java.util.stream.Collectors;

public class TrancaRepositorio {
    public Tranca salvarTranca(PostTranca postTranca) {
        Validador.validarCampos(postTranca);
        Tranca tranca = new Tranca();
        String id = UUID.randomUUID().toString();
        tranca.setId(id);
        tranca.setStatus(postTranca.getStatus());
        tranca.setLocalizacao(postTranca.getLocalizacao());
        tranca.setModelo(postTranca.getModelo());
        tranca.setAnoDeFabricacao(postTranca.getAnoDeFabricacao());
        tranca.setNumero(String.valueOf(postTranca.getNumero()));
        BancoDeDados.TRANCA_TABLE.put(id, tranca);
        return tranca;
    }

    public List<Tranca> buscarTrancas() {
        return BancoDeDados.TRANCA_TABLE.keySet()
                .stream()
                .map(BancoDeDados.TRANCA_TABLE::get)
                .collect(Collectors.toList());
    }

    public Tranca buscarTranca(String id) {
        Tranca tranca = BancoDeDados.TRANCA_TABLE.get(id);
        if (tranca == null) throw new GenericApiException(404, "Tranca não encontrado");
        return tranca;
    }

    public Tranca atualizarTranca(String id, Tranca novaTranca){
        Validador.validarCampos(novaTranca);
        buscarTranca(id);
        BancoDeDados.TRANCA_TABLE.replace(id, novaTranca);
        return novaTranca;
    }

    public void deletarTranca(String id){
        Tranca tranca = buscarTranca(id);
        if (tranca.getIdBicicleta() != null) throw new GenericApiException(422, "Tranca não pode ser removida poís possui uma bicicleta.");
        BancoDeDados.TRANCA_TABLE.remove(id);
    }

    public Bicicleta buscarBicicletaPorTranca(String id) {
        Tranca tranca = buscarTranca(id);
        return Optional.ofNullable(BancoDeDados.BICICLETA_TABLE.get(tranca.getIdBicicleta()))
                .orElseThrow(() -> new GenericApiException(422, "Bicicleta não encontrado."));
    }

    public Tranca alterarStatus(String id, String status){
        Tranca tranca = buscarTranca(id);
        tranca.setStatus(TrancaStatus.valueOf(status));
        return BancoDeDados.TRANCA_TABLE.replace(tranca.getId(), tranca);
    }

    public void integrarNaRede(TrancaNaRede trancaNaRede) {
        Validador.validarCampos(trancaNaRede);
        Tranca tranca = buscarTranca(trancaNaRede.getIdTranca());
        Totem totem = buscarTotem(trancaNaRede.getIdTotem());
        tranca.setIdTotem(totem.getId());
//        tranca.setStatus(TrancaStatus.DISPONIVEL);
        BancoDeDados.TRANCA_TABLE.replace(tranca.getId(), tranca);
    }

    public void removerDaRede(TrancaNaRede trancaNaRede) {
        Validador.validarCampos(trancaNaRede);
        Tranca tranca = buscarTranca(trancaNaRede.getIdTranca());
        if (tranca.getIdBicicleta() != null) throw new GenericApiException(422, "Tranca possui uma bicicleta.");
        buscarTotem(trancaNaRede.getIdTotem());
        tranca.setIdTotem(null);

    }

    private Totem buscarTotem(String id) {
        return Optional.ofNullable(BancoDeDados.TOTEM_TABLE.get(id))
                .orElseThrow(() -> new GenericApiException(422, "Totem não encontrado."));
    }
}

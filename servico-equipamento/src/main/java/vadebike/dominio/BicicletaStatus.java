package vadebike.dominio;

public enum BicicletaStatus {
    DISPONIVEL, APOSENTADA
}

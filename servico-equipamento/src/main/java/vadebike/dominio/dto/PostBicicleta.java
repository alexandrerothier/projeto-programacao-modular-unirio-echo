package vadebike.dominio.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import vadebike.dominio.BicicletaStatus;

public class PostBicicleta {
    @NotBlank(message = "O campo 'marca' é obrigatório.")
    private String marca;
    @NotBlank(message = "O campo 'modelo' é obrigatório.")
    private String modelo;
    @NotBlank(message = "O campo 'ano' é obrigatório.")
    private String ano;
    @NotNull(message = "O campo 'numero' é obrigatório.")
    private Integer numero;
    @NotNull(message = "O campo 'status' é obrigatório.")
    private BicicletaStatus status;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public BicicletaStatus getStatus() {
        return status;
    }

    public void setStatus(BicicletaStatus status) {
        this.status = status;
    }
}

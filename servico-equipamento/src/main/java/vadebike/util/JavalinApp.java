package vadebike.util;

import io.javalin.plugin.json.JavalinJson;
import vadebike.controller.BicicletaController;
import vadebike.controller.TotemController;
import vadebike.controller.TrancaController;
import io.javalin.Javalin;
import vadebike.excecoes.GenericApiException;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    private final Javalin app =
            Javalin.create(config -> config.defaultContentType = "application/json")
                    .routes(() -> {
                        path("/totem", () -> get(TotemController::buscarTotens));
                        path("/totem", () -> post(TotemController::salvarTotem));
                        path("/totem/:idTotem", () -> delete(TotemController::deletarTotem));
                        path("/totem/:idTotem", () -> put(TotemController::atualizarTotem));
                        path("/totem/:idTotem/trancas", () -> get(TotemController::buscarTrancasPorTotem));
                        path("/totem/:idTotem/bicicletas", () -> get(TotemController::buscarBicicletasPorTotem));

                        path("/tranca", () -> get(TrancaController::buscarTrancas));
                        path("/tranca", () -> post(TrancaController::salvarTranca));
                        path("/tranca/integrarNaRede", () -> post(TrancaController::integrarNaRede));
                        path("/tranca/removerDaRede", () -> post(TrancaController::removerDaRede));
                        path("/tranca/:idTranca", () -> get(TrancaController::buscarTranca));
                        path("/tranca/:idTranca", () -> delete(TrancaController::deletarTranca));
                        path("/tranca/:idTranca", () -> put(TrancaController::atualizarTranca));
                        path("/tranca/:idTranca/bicicleta", () -> get(TrancaController::buscaBicicletaPorTranca));
                        path("/tranca/:idTranca/status/:acao", () -> post(TrancaController::alterarStatus));

                        path("/bicicleta", () -> get(BicicletaController::buscarBicicletas));
                        path("/bicicleta", () -> post(BicicletaController::salvarBicicleta));
                        path("/bicicleta/integrarNaRede", () -> post(BicicletaController::integrarNaRede));
                        path("/bicicleta/removerDaRede", () -> post(BicicletaController::removerDaRede));
                        path("/bicicleta/:idBicicleta", () -> get(BicicletaController::buscarBicicleta));
                        path("/bicicleta/:idBicicleta", () -> delete(BicicletaController::deletarBicicleta));
                        path("/bicicleta/:idBicicleta", () -> put(BicicletaController::atualizarBicicleta));
                        path("/bicicleta/:idBicicleta/status/:acao", () -> post(BicicletaController::alterarStatus));
                    })
                    .error(500, ctx -> {
                        GenericApiException ex = new GenericApiException(500, "Ocorreu um erro durante a operação");
                        ctx.result(JavalinJson.toJson(ex));
                    }).error(404, ctx -> {
                        if (ctx.attribute("handle") == null) {
                            GenericApiException ex = new GenericApiException(404, "Página não encontrada");
                            ctx.result(JavalinJson.toJson(ex));
                        }
                    })
                    .exception(GenericApiException.class, (e, ctx) -> {
                        ctx.attribute("handle", true);
                        String response = JavalinJson.getToJsonMapper().map(e);
                        ctx.status(e.getCodigo());
                        ctx.result(response);
                    });



    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}

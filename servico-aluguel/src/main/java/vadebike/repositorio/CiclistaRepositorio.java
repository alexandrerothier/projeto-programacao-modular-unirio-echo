package vadebike.repositorio;

import vadebike.banco.BancoDeDados;
import vadebike.dominio.*;
import vadebike.excecao.GenericApiException;
import vadebike.servico.ServicoExterno;
import vadebike.util.Validador;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class CiclistaRepositorio {
    private final ServicoExterno servicoExterno;

    public CiclistaRepositorio(ServicoExterno servicoExterno) {
        this.servicoExterno = servicoExterno;
    }


    public Emprestimo alugar(RequisicaoAluguel requisicaoAluguel) {
        Validador.validarCampos(requisicaoAluguel);
        Tranca tranca = servicoExterno.buscarTranca(requisicaoAluguel.getTrancaInicio());
        Bicicleta bicicleta = servicoExterno.buscarBicicleta(tranca.getBicicleta());
        Ciclista ciclista = buscarCiclista(requisicaoAluguel.getCiclista());
        RequisicaoCobranca requisicaoCobranca = new RequisicaoCobranca();
        requisicaoCobranca.setCiclista(ciclista.getId());
        requisicaoCobranca.setValor(10.0);
        Cobranca cobranca = servicoExterno.realizarCobranca(requisicaoCobranca);
        servicoExterno.mudarStatusBicicleta(bicicleta.getId(), "OCUPADA");
        servicoExterno.mudarStatusTranca(tranca.getId(), "DISPONIVEL");
        Emprestimo emprestimo = new Emprestimo();
        emprestimo.setCiclista(ciclista.getId());
        emprestimo.setBicicletaId(bicicleta.getId());
        emprestimo.setCobranca(cobranca.getId());
        emprestimo.setHoraInicio(new Date());
        emprestimo.setTrancaInicio(tranca.getId());
        BancoDeDados.TABELA_EMPRESTIMO.put(UUID.randomUUID().toString(), emprestimo);
        return emprestimo;
    }

    public Emprestimo devolver(RequisicaoDevolucao requisicaoDevolucao) {
        Bicicleta bicicleta = servicoExterno.buscarBicicleta(requisicaoDevolucao.getIdBicicleta());
        Tranca tranca = servicoExterno.buscarTranca(requisicaoDevolucao.getIdTranca());
        Emprestimo emprestimo = BancoDeDados.TABELA_EMPRESTIMO.keySet()
                .stream()
                .map(BancoDeDados.TABELA_EMPRESTIMO::get)
                .filter(e -> e.getBicicletaId().equals(bicicleta.getId()))
                .findFirst().orElseThrow(() -> new GenericApiException(422, "Bicicleta não está alugada"));

        double tempo = (new Date().getTime() - emprestimo.getHoraInicio().getTime()) / 1000.0 / 60.0; // em minutos
        if (tempo > 150) {
            double excesso = tempo - 120.0;
            RequisicaoCobranca requisicaoCobranca = new RequisicaoCobranca();
            requisicaoCobranca.setCiclista(emprestimo.getCiclista());
            requisicaoCobranca.setValor(5.0 * (excesso / 30.0));
            servicoExterno.realizarCobranca(requisicaoCobranca);
        }
        servicoExterno.mudarStatusBicicleta(bicicleta.getId(), "DISPONIVEL");
        servicoExterno.mudarStatusTranca(tranca.getId(), "OCUPADA");
        emprestimo.setHoraFim(new Date());
        emprestimo.setTrancaFim(tranca.getId());
        return emprestimo;
    }

    public Ciclista buscarCiclista(String id) {
        Ciclista ciclista = BancoDeDados.TABELA_CICLISTA.get(id);
        if (ciclista == null) throw new GenericApiException(404, "Ciclista não encontrado");
        return ciclista;
    }

    public MeioDePagamento buscarCartaoPorCiclista(String idCiclista) {
        Ciclista ciclista = buscarCiclista(idCiclista);
        return ciclista.getMeioDePagamento();
    }

    public MeioDePagamento atualizarCartaoPorCiclista(String idCiclista, MeioDePagamento novoMeioDePagamento) {
        Validador.validarCampos(novoMeioDePagamento);
        Ciclista ciclista = buscarCiclista(idCiclista);
        ciclista.setMeioDePagamento(novoMeioDePagamento);
        BancoDeDados.TABELA_CICLISTA.replace(idCiclista, ciclista);
        return novoMeioDePagamento;
    }

    public CiclistaCorpoApi atualizar(String id, CiclistaCorpoApi ciclistaCorpoApi) {
        Validador.validarCampos(ciclistaCorpoApi);
        Ciclista ciclista = buscarCiclista(id);
        ciclista.setCpf(ciclistaCorpoApi.getCpf());
        ciclista.setNascimento(ciclistaCorpoApi.getNascimento());
        ciclista.setNome(ciclistaCorpoApi.getNome());
        ciclista.setEmail(ciclistaCorpoApi.getEmail());
        ciclista.setNacionalidade(ciclistaCorpoApi.getNacionalidade());
        ciclista.setPassaporte(ciclistaCorpoApi.getPassaporte());
        ciclista.setStatus(ciclistaCorpoApi.getStatus());
        Validador.validarEstrangeiro(ciclista);
        BancoDeDados.TABELA_CICLISTA.put(id, ciclista);
        ciclistaCorpoApi.setId(id);
        return ciclistaCorpoApi;
    }

    public boolean existeCiclistaPorEmail(String email) {
        return buscarPorEmail(email).isPresent();
    }

    private Optional<Ciclista> buscarPorEmail(String email) {
        return BancoDeDados.TABELA_CICLISTA.keySet()
                .stream()
                .map(BancoDeDados.TABELA_CICLISTA::get)
                .filter((c) -> c.getEmail().equals(email))
                .findFirst();
    }

    public Ciclista cadastrar(Ciclista ciclista) {
        Validador.validarCampos(ciclista);
        Validador.validarEstrangeiro(ciclista);
        servicoExterno.validarCartao(ciclista.getMeioDePagamento());
        String id = UUID.randomUUID().toString();
        ciclista.setId(id);
        BancoDeDados.TABELA_CICLISTA.put(id, ciclista);
        servicoExterno.enviarEmail(montarEmail(ciclista.getEmail()));
        return ciclista;
    }

    public CiclistaCorpoApi confirmarEmail(String id) {
        Ciclista ciclista = buscarCiclista(id);
        if (ciclista.getStatus()) throw new GenericApiException(422, "Esta conta já foi confirmada.");
        ciclista.setStatus(true);
        CiclistaCorpoApi ciclistaCorpoApi = new CiclistaCorpoApi();
        ciclistaCorpoApi.setId(ciclista.getId());
        ciclistaCorpoApi.setEmail(ciclista.getEmail());
        ciclistaCorpoApi.setCpf(ciclista.getCpf());
        ciclistaCorpoApi.setNome(ciclista.getNome());
        ciclistaCorpoApi.setNacionalidade(ciclista.getNacionalidade());
        ciclistaCorpoApi.setStatus(ciclista.getStatus());
        ciclistaCorpoApi.setPassaporte(ciclista.getPassaporte());
        return ciclistaCorpoApi;
    }

    private Email montarEmail(String destinatario) {
        String url = ServicoExterno.BASE_URL;
        String mensagem = String.format("Obrigado por usar nossos serviços. Por favor, confirme seu cadastramento " +
                "clicando no link\n\n %s/%s/ativar", url, destinatario);
        Email email = new Email();
        email.setEmail(destinatario);
        email.setMensagem(mensagem);
        return email;
    }
}

package vadebike.servico;

import vadebike.dominio.*;

import java.util.Date;

public class ServicoExterno {
    public static final String BASE_URL;

    static {
        String urlExterno = System.getenv("URL_EXTERNO");
        if(urlExterno == null) urlExterno = "http://localhost:7002";
        BASE_URL = urlExterno;
    }

    public boolean validarCartao(MeioDePagamento meioDePagamento) {
        // TODO Integração
        return true;
    }

    public Cobranca realizarCobranca(RequisicaoCobranca requisicaoCobranca) {
        // TODO Integração
        Cobranca cobranca = new Cobranca();
        cobranca.setId("123");
        cobranca.setStatus("APPROVED");
        return cobranca;
    }

    public void mudarStatusBicicleta(String id, String status){
        // TODO Integração
    }

    public void mudarStatusTranca(String id, String status){
        // TODO Integração
    }

    public void enviarEmail(Email email) {
        // TODO Integração
    }

    public Tranca buscarTranca(String id) {
        // TODO Integração
        Tranca tranca = new Tranca();
        tranca.setId("123");
        tranca.setBicicleta("123");
        tranca.setStatus("OCUPADA");
        return tranca;
    }

    public Bicicleta buscarBicicleta(String id) {
        // TODO Integração
        Bicicleta bicicleta = new Bicicleta();
        bicicleta.setId("123");
        bicicleta.setStatus("DISPONIVEL");
        return bicicleta;
    }
}

package vadebike.dominio;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.awt.*;
import java.util.Date;
import java.util.Objects;

public class Emprestimo {
    private String bicicletaId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Sao_Paulo")
    private Date horaInicio;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Sao_Paulo")

    private Date horaFim;
    private String trancaInicio;
    private String trancaFim;
    private String cobranca;
    private String ciclista;

    @Override
    public String toString() {
        return "Emprestimo{" +
                "bicicletaId='" + bicicletaId + '\'' +
                ", horaInicio=" + horaInicio +
                ", horaFim=" + horaFim +
                ", trancaInicio='" + trancaInicio + '\'' +
                ", trancaFim='" + trancaFim + '\'' +
                ", cobranca='" + cobranca + '\'' +
                ", ciclista='" + ciclista + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Emprestimo that = (Emprestimo) o;
        return Objects.equals(bicicletaId, that.bicicletaId) && Objects.equals(horaInicio, that.horaInicio) && Objects.equals(horaFim, that.horaFim) && Objects.equals(trancaInicio, that.trancaInicio) && Objects.equals(trancaFim, that.trancaFim) && Objects.equals(cobranca, that.cobranca) && Objects.equals(ciclista, that.ciclista);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bicicletaId, horaInicio, horaFim, trancaInicio, trancaFim, cobranca, ciclista);
    }

    public String getBicicletaId() {
        return bicicletaId;
    }

    public void setBicicletaId(String bicicletaId) {
        this.bicicletaId = bicicletaId;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(Date horaFim) {
        this.horaFim = horaFim;
    }

    public String getTrancaInicio() {
        return trancaInicio;
    }

    public void setTrancaInicio(String trancaInicio) {
        this.trancaInicio = trancaInicio;
    }

    public String getTrancaFim() {
        return trancaFim;
    }

    public void setTrancaFim(String trancaFim) {
        this.trancaFim = trancaFim;
    }

    public String getCobranca() {
        return cobranca;
    }

    public void setCobranca(String cobranca) {
        this.cobranca = cobranca;
    }

    public String getCiclista() {
        return ciclista;
    }

    public void setCiclista(String ciclista) {
        this.ciclista = ciclista;
    }
}

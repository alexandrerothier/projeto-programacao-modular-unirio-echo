package vadebike.banco;

import vadebike.dominio.Ciclista;
import vadebike.dominio.Emprestimo;
import vadebike.dominio.Funcionario;
import vadebike.dominio.MeioDePagamento;

import java.util.*;

public class BancoDeDados {
    public static final Map<String, Ciclista> TABELA_CICLISTA = new Hashtable<>();
    public static final Map<String, Funcionario> TABELA_FUNCIONARIO = new Hashtable<>();
    public static final Map<String, Emprestimo> TABELA_EMPRESTIMO = new Hashtable<>();

    public static void limpar() {
        TABELA_CICLISTA.clear();
        TABELA_FUNCIONARIO.clear();
    }

    public static void carregarDados() {
        limpar();
        Ciclista ciclista = getCiclista();
        Funcionario funcionario = getFuncionario();
        TABELA_CICLISTA.put(ciclista.getId(), ciclista);
        TABELA_FUNCIONARIO.put(funcionario.getId(), funcionario);
    }
    private static Ciclista getCiclista() {
        MeioDePagamento meioDePagamento = new MeioDePagamento();
        meioDePagamento.setNomeTitular("123");
        meioDePagamento.setValidade(new GregorianCalendar(2023, Calendar.JANUARY,1).getTime());
        meioDePagamento.setNumero("12345678910123");
        meioDePagamento.setCvv("123");

        Ciclista ciclista = new Ciclista();
        ciclista.setId("123");
        ciclista.setNome("123");
        ciclista.setCpf("99999999999");
        ciclista.setEmail("vadebike@email.com");
        ciclista.setNacionalidade("BRASIL");
        ciclista.setSenha("123");
        ciclista.setStatus(true);
        ciclista.setNascimento(new GregorianCalendar(1999, Calendar.JANUARY, 30).getTime());
        ciclista.setMeioDePagamento(meioDePagamento);
        return ciclista;
    }

    private static Funcionario getFuncionario() {
        Funcionario funcionario = new Funcionario();
        funcionario.setNome("123");
        funcionario.setId("123");
        funcionario.setIdade(123);
        funcionario.setCpf("99999999999");
        funcionario.setEmail("vadebike@email.com");
        funcionario.setSenha("123");
        funcionario.setFuncao("123");
        return funcionario;
    }
}

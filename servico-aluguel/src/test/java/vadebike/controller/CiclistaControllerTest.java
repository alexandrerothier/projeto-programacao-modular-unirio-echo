package vadebike.controller;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.junit.jupiter.api.*;
import vadebike.banco.BancoDeDados;
import vadebike.dominio.*;
import vadebike.excecao.GenericApiException;
import vadebike.repositorio.CiclistaRepositorio;
import vadebike.servico.ServicoExterno;
import vadebike.util.JavalinApp;

import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;
import static vadebike.modelos.CiclistaModelos.*;
import static vadebike.modelos.CiclistaModelos.getCiclista;

class CiclistaControllerTest {

    private final static JavalinApp app = new JavalinApp();
    private static final String BASE_URL = "http://localhost:7010";

    private final CiclistaRepositorio ciclistaRepositorio = new CiclistaRepositorio(new ServicoExterno());

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    @BeforeEach
    void setUp() {
        BancoDeDados.carregarDados();
    }

    @AfterAll
    static void tearDown() {
        BancoDeDados.limpar();
        app.stop();
    }


    @Test
    void alugar_RetornaEmprestimo_QuandoBemSucedido() {
        Bicicleta bicicleta = getBicicleta();
        Cobranca cobranca = getCobranca();
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/aluguel")
                .body(requisicaoAluguel)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(201, response.getStatus());
        assertEquals(requisicaoAluguel.getCiclista(), jsonResponse.get("ciclista"));
        assertEquals(requisicaoAluguel.getTrancaInicio(), jsonResponse.get("trancaInicio"));
        assertEquals(bicicleta.getId(), jsonResponse.get("bicicletaId"));
        assertEquals(cobranca.getId(), jsonResponse.get("cobranca"));
    }

    @Test
    void devolver_RetornaEmprestimo_QuandoBemSucedido() {
        RequisicaoAluguel requisicaoAluguel = getRequisicaoAluguel();
        RequisicaoDevolucao requisicaoDevolucao = new RequisicaoDevolucao();
        requisicaoDevolucao.setIdBicicleta("123");
        requisicaoDevolucao.setIdTranca("123");
        Emprestimo emprestimo = ciclistaRepositorio.alugar(requisicaoAluguel);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/devolucao")
                .body(requisicaoDevolucao)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(201, response.getStatus());
        assertEquals(emprestimo.getCiclista(), jsonResponse.get("ciclista"));
        assertEquals(emprestimo.getTrancaInicio(), jsonResponse.get("trancaInicio"));
        assertEquals(emprestimo.getBicicletaId(), jsonResponse.get("bicicletaId"));
        assertEquals(emprestimo.getCobranca(), jsonResponse.get("cobranca"));
        assertEquals(requisicaoDevolucao.getIdTranca(), jsonResponse.get("trancaFim"));
    }



    @Test
    void existeEmail_RetornaVerdadeiro_QuandoExiste() {
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/existeEmail/vadebike@email.com")
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("true", response.getBody());
    }

    @Test
    void existeEmail_RetornaFalso_QuandoNaoExiste() {
        HttpResponse<String> response = Unirest.get(BASE_URL + "/ciclista/existeEmail/xxx@email.com")
                .asString();

        assertEquals(200, response.getStatus());
        assertEquals("false", response.getBody());
    }

    @Test
    void cadastrarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        ciclista.setId(null);
        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(201, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonResponse.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonResponse.get("nacionalidade"));
        assertEquals(new SimpleDateFormat("yyyy-MM-dd").format(ciclista.getNascimento()), jsonResponse.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonResponse.get("cpf"));
        assertEquals(ciclista.getNome(), jsonResponse.get("nome"));
        assertEquals(ciclista.getStatus(), jsonResponse.get("status"));
        assertEquals(ciclista.getSenha(), jsonResponse.get("senha"));

//        assertEquals(ciclista.getPassaporte().getNumero(), jsonResponse.getJSONObject("passaporte").get("numero"));
//        assertEquals(ciclista.getPassaporte().getPais(), jsonResponse.getJSONObject("passaporte").get("pais"));
//        assertEquals(ciclista.getPassaporte().getValidade(), jsonResponse.getJSONObject("passaporte").get("validade"));

        assertEquals(ciclista.getMeioDePagamento().getCvv(), jsonResponse.getJSONObject("meioDePagamento").get("cvv"));
        assertEquals(new SimpleDateFormat("yyyy-MM").format(ciclista.getMeioDePagamento().getValidade()), jsonResponse.getJSONObject("meioDePagamento").get("validade"));
        assertEquals(ciclista.getMeioDePagamento().getNumero(), jsonResponse.getJSONObject("meioDePagamento").get("numero"));
        assertEquals(ciclista.getMeioDePagamento().getNomeTitular(), jsonResponse.getJSONObject("meioDePagamento").get("nomeTitular"));

    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoEstrangeiroSemPassaporte() {
        Ciclista ciclista = getCiclista();
        ciclista.setId(null);
        ciclista.setNacionalidade("USA");
        ciclista.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(422, response.getStatus());
        assertEquals("O campo 'passaporte' é obrigatório para usuários estrangeiros", jsonResponse.get("mensagem"));
    }

    @Test
    void cadastrarCiclista_LancaExcecao_QuandoBrasileiroSemCpf() {
        Ciclista ciclista = getCiclista();
        ciclista.setId(null);
        ciclista.setCpf(null);

        HttpResponse<JsonNode> response = Unirest.post(BASE_URL + "/ciclista")
                .body(ciclista)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(422, response.getStatus());
        assertEquals("O campo 'cpf' é obrigatório para usuários brasileiros", jsonResponse.get("mensagem"));
    }

    @Test
    void atualizarCiclista_RetornaCiclista_QuandoBemSucedido() {
        CiclistaCorpoApi ciclistaCorpoApi = getCiclistaCorpoApi();
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/ciclista/123")
                .body(ciclistaCorpoApi)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(ciclistaCorpoApi.getEmail(), jsonResponse.get("email"));
        assertEquals(ciclistaCorpoApi.getNacionalidade(), jsonResponse.get("nacionalidade"));
        assertEquals(new SimpleDateFormat("yyyy-MM-dd").format(ciclistaCorpoApi.getNascimento()), jsonResponse.get("nascimento"));
        assertEquals(ciclistaCorpoApi.getCpf(), jsonResponse.get("cpf"));
        assertEquals(ciclistaCorpoApi.getNome(), jsonResponse.get("nome"));
        assertEquals(ciclistaCorpoApi.getStatus(), jsonResponse.get("status"));

//        assertEquals(ciclista.getPassaporte().getNumero(), jsonResponse.getJSONObject("passaporte").get("numero"));
//        assertEquals(ciclista.getPassaporte().getPais(), jsonResponse.getJSONObject("passaporte").get("pais"));
//        assertEquals(ciclista.getPassaporte().getValidade(), jsonResponse.getJSONObject("passaporte").get("validade"));
    }

    @Test
    void buscarCiclista_LancaExcecao_QuandoNaoEncontrado() {
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/000")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(404, response.getStatus());
        assertEquals("Ciclista não encontrado", jsonResponse.get("mensagem"));
    }

    @Test
    void buscarCiclista_RetornaCiclista_QuandoBemSucedido() {
        Ciclista ciclista = getCiclista();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/123")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(ciclista.getEmail(), jsonResponse.get("email"));
        assertEquals(ciclista.getNacionalidade(), jsonResponse.get("nacionalidade"));
        assertEquals(new SimpleDateFormat("yyyy-MM-dd").format(ciclista.getNascimento()), jsonResponse.get("nascimento"));
        assertEquals(ciclista.getCpf(), jsonResponse.get("cpf"));
        assertEquals(ciclista.getNome(), jsonResponse.get("nome"));
        assertEquals(ciclista.getStatus(), jsonResponse.get("status"));
        assertEquals(ciclista.getSenha(), jsonResponse.get("senha"));

//        assertEquals(ciclista.getPassaporte().getNumero(), jsonResponse.getJSONObject("passaporte").get("numero"));
//        assertEquals(ciclista.getPassaporte().getPais(), jsonResponse.getJSONObject("passaporte").get("pais"));
//        assertEquals(ciclista.getPassaporte().getValidade(), jsonResponse.getJSONObject("passaporte").get("validade"));

        assertEquals(ciclista.getMeioDePagamento().getCvv(), jsonResponse.getJSONObject("meioDePagamento").get("cvv"));
        assertEquals(new SimpleDateFormat("yyyy-MM").format(ciclista.getMeioDePagamento().getValidade()), jsonResponse.getJSONObject("meioDePagamento").get("validade"));
        assertEquals(ciclista.getMeioDePagamento().getNumero(), jsonResponse.getJSONObject("meioDePagamento").get("numero"));
        assertEquals(ciclista.getMeioDePagamento().getNomeTitular(), jsonResponse.getJSONObject("meioDePagamento").get("nomeTitular"));
    }

    @Test
    void buscarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido() {
        MeioDePagamento meioDePagamento = getCiclista().getMeioDePagamento();
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/cartaoDeCredito/123")
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(meioDePagamento.getCvv(), jsonResponse.get("cvv"));
        assertEquals(new SimpleDateFormat("yyyy-MM").format(meioDePagamento.getValidade()), jsonResponse.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonResponse.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonResponse.get("nomeTitular"));
    }

    @Test
    void atualizarCartaoPorCiclista_RetornaCartao_QuandoBemSucedido() {
        MeioDePagamento meioDePagamento = getCiclista().getMeioDePagamento();
        meioDePagamento.setNomeTitular("ALTERADO");
        HttpResponse<JsonNode> response = Unirest.put(BASE_URL + "/cartaoDeCredito/123")
                .body(meioDePagamento)
                .asJson();

        JSONObject jsonResponse = response.getBody().getObject();

        assertEquals(200, response.getStatus());
        assertEquals(meioDePagamento.getCvv(), jsonResponse.get("cvv"));
        assertEquals(new SimpleDateFormat("yyyy-MM").format(meioDePagamento.getValidade()), jsonResponse.get("validade"));
        assertEquals(meioDePagamento.getNumero(), jsonResponse.get("numero"));
        assertEquals(meioDePagamento.getNomeTitular(), jsonResponse.get("nomeTitular"));
    }

    @Test
    void confirmarEmail_MudaStatusParaConfirmado_QuandoBemSucedido() {
        CiclistaCorpoApi ciclistaCorpoApi = getCiclistaCorpoApi();
        ciclistaCorpoApi.setStatus(false);
        ciclistaRepositorio.atualizar(ciclistaCorpoApi.getId(), ciclistaCorpoApi);
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/123/ativar")
                .asJson();

        assertEquals(200, response.getStatus());
    }

    @Test
    void confirmarEmail_LancaExcecao_QuandoJaConfirmado() {
        HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "/ciclista/123/ativar")
                .asJson();

        assertEquals(422, response.getStatus());
        assertEquals("Esta conta já foi confirmada.", response.getBody().getObject().get("mensagem"));
    }
}
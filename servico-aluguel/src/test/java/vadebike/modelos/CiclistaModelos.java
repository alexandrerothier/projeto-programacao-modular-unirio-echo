package vadebike.modelos;

import vadebike.dominio.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CiclistaModelos {

    public static Ciclista getCiclista() {
        MeioDePagamento meioDePagamento = new MeioDePagamento();
        meioDePagamento.setNomeTitular("123");
        meioDePagamento.setValidade(new GregorianCalendar(2023, Calendar.JANUARY, 1).getTime());
        meioDePagamento.setNumero("12345678910123");
        meioDePagamento.setCvv("123");

        Ciclista ciclista = new Ciclista();
        ciclista.setId("123");
        ciclista.setNome("123");
        ciclista.setCpf("99999999999");
        ciclista.setEmail("vadebike@email.com");
        ciclista.setNacionalidade("BRASIL");
        ciclista.setSenha("123");
        ciclista.setStatus(true);
        ciclista.setNascimento(new GregorianCalendar(1999, Calendar.JANUARY, 30).getTime());
        ciclista.setMeioDePagamento(meioDePagamento);
        return ciclista;
    }

    public static CiclistaCorpoApi getCiclistaCorpoApi() {
        CiclistaCorpoApi ciclistaCorpoApi = new CiclistaCorpoApi();
        ciclistaCorpoApi.setId("123");
        ciclistaCorpoApi.setNome("123");
        ciclistaCorpoApi.setCpf("99999999999");
        ciclistaCorpoApi.setEmail("vadebike@email.com");
        ciclistaCorpoApi.setNacionalidade("BRASIL");
        ciclistaCorpoApi.setStatus(true);
        ciclistaCorpoApi.setNascimento(new Date());
        return ciclistaCorpoApi;
    }

    public static Bicicleta getBicicleta() {
        Bicicleta bicicleta = new Bicicleta();
        bicicleta.setStatus("DISPONIVEL");
        bicicleta.setId("123");
        return bicicleta;
    }

    public static Tranca getTranca() {
        Tranca tranca = new Tranca();
        tranca.setStatus("OCUPADA");
        tranca.setId("123");
        tranca.setBicicleta("123");
        return tranca;
    }

    public static Cobranca getCobranca() {
        Cobranca cobranca = new Cobranca();
        cobranca.setStatus("APPROVED");
        cobranca.setId("123");
        return cobranca;
    }

    public static RequisicaoAluguel getRequisicaoAluguel() {
        RequisicaoAluguel requisicaoAluguel = new RequisicaoAluguel();
        requisicaoAluguel.setCiclista("123");
        requisicaoAluguel.setTrancaInicio("123");
        return requisicaoAluguel;
    }
}

package vadebike.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class Segredos {
    private static Properties properties;
    private static final String EMAIL_API_KEY = "EMAIL_API_KEY";
    private static final String EMAIL_DOMAIN = "EMAIL_DOMAIN";
    private static final String CLIENT_ID = "CLIENT_ID";
    private static final String CLIENT_SECRET = "CLIENT_SECRET";
    private static final String SELLER_ID = "SELLER_ID";

    static {
        Path path = Paths.get("segredos.properties");
        if (Files.exists(path)) {
            try {
                FileInputStream fileInputStream = new FileInputStream("segredos.properties");
                properties = new Properties();
                properties.load(fileInputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static String getEmailApiKey() {
        String emailApiKey = System.getenv(EMAIL_API_KEY);
        if (emailApiKey == null) emailApiKey = properties.getProperty(EMAIL_API_KEY);
        return emailApiKey;
    }

    public static String getEmailDomainName() {
        String emailDomainName = System.getenv(EMAIL_DOMAIN);
        if (emailDomainName == null) emailDomainName = properties.getProperty(EMAIL_DOMAIN);
        return emailDomainName;
    }

    public static String getClientId() {
        String clientId = System.getenv(CLIENT_ID);
        if (clientId == null) clientId = properties.getProperty(CLIENT_ID);
        return clientId;
    }

    public static String getClientSecret() {
        String clientSecret = System.getenv(CLIENT_SECRET);
        if (clientSecret == null) clientSecret = properties.getProperty(CLIENT_SECRET);
        return clientSecret;
    }

    public static String getSellerId() {
        String clientSecret = System.getenv(SELLER_ID);
        if (clientSecret == null) clientSecret = properties.getProperty(SELLER_ID);
        return clientSecret;
    }
}

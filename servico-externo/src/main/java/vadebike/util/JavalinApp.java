package vadebike.util;

import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJson;
import vadebike.controller.EmailController;
import vadebike.controller.PagamentoController;
import vadebike.excecoes.GenericApiException;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    private final Javalin app =
            Javalin.create(config -> config.defaultContentType = "application/json")
                    .routes(() -> {
                        path("/enviarEmail", () -> post(EmailController::enviarEmail));
                        path("/cobranca", () -> post(PagamentoController::realizarCobranca));
                        path("/filaCobranca", () -> post(PagamentoController::adicionarNaFilaCobranca));
                        path("/cobranca/:idCobranca", () -> get(PagamentoController::buscarCobranca));
                        path("/validarCartaoDeCredito", () -> post(PagamentoController::validarCartao));

                    }).error(500, ctx -> {
                        GenericApiException ex = new GenericApiException(500, "Ocorreu um erro durante a operação");
                        ctx.result(JavalinJson.toJson(ex));
                    }).error(404, ctx -> {
                        if (ctx.attribute("handle") == null) {
                            GenericApiException ex = new GenericApiException(404, "Página não encontrada");
                            ctx.result(JavalinJson.toJson(ex));
                        }
                    })
                    .exception(GenericApiException.class, (e, ctx) -> {
                        ctx.attribute("handle", true);
                        String response = JavalinJson.getToJsonMapper().map(e);
                        ctx.status(e.getCodigo());
                        ctx.result(response);
                    });


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}

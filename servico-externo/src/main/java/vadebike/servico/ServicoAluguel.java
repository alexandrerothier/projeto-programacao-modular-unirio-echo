package vadebike.servico;

import vadebike.dominio.Cartao;
import vadebike.dominio.Ciclista;

public class ServicoAluguel {
    
    public Ciclista getCiclista(String id) {
        Ciclista ciclista = new Ciclista();
        Cartao cartao = new Cartao();
        cartao.setNumero("5155901222280001");
        cartao.setCvv("123");
        cartao.setValidade("2023-07-11");
        cartao.setNomeTitular("Pedro");
        ciclista.setId(id);
        ciclista.setCartao(cartao);
        return ciclista;
    }
}

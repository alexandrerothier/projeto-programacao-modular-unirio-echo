package vadebike;

import vadebike.banco.BancoDeDados;
import vadebike.util.JavalinApp;

public class Main {
    public static void main(String[] args) {
        JavalinApp app = new JavalinApp();
        BancoDeDados.carregarDados();
        app.start(getHerokuAssignedPort());
    }
    private static int getHerokuAssignedPort() {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
          return Integer.parseInt(herokuPort);
        }
        return 7002;
      }
}

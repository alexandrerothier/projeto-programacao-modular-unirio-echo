package vadebike.banco;

import vadebike.dominio.Cobranca;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class BancoDeDados {

    private BancoDeDados() {}
    public static final Map<String, Cobranca> COBRANCA_TABLE = new HashMap<>();

    public static void limpar() {
        COBRANCA_TABLE.clear();
    }

    public static void carregarDados() {
        limpar();
        Cobranca cobranca = new Cobranca();
        cobranca.setId("123");
        cobranca.setValor(123.0);
        cobranca.setCiclista("123");
        cobranca.setStatus("APPROVED");
        cobranca.setHoraSolicitacao(new GregorianCalendar(2020, Calendar.JANUARY, 1, 12, 30, 0).getTime());
        cobranca.setHoraFinalizacao(new GregorianCalendar(2020, Calendar.JANUARY, 1, 12, 30, 0).getTime());
        COBRANCA_TABLE.put(cobranca.getId(), cobranca);
    }
}

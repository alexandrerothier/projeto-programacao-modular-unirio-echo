package vadebike.excecoes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.UUID;
@JsonIgnoreProperties({"message", "suppressed", "localizedMessage", "stackTrace", "cause"})
public class GenericApiException extends RuntimeException {
    private final String id;
    private final Integer codigo;
    private final String mensagem;

    public GenericApiException(Integer codigo, String mensagem) {
        super(mensagem);
        this.id = UUID.randomUUID().toString();
        this.codigo = codigo;
        this.mensagem = mensagem;
    }

    public String getId() {
        return id;
    }


    public Integer getCodigo() {
        return codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

}

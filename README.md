## Serviço Externo
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-externo&metric=coverage)](https://sonarcloud.io/summary/new_code?id=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-externo)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-externo&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-externo)
## Serviço Equipamento
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-equipamento&metric=coverage)](https://sonarcloud.io/summary/new_code?id=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-equipamento)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-equipamento&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-equipamento)
## Serviço Aluguel
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-aluguel&metric=coverage)](https://sonarcloud.io/summary/new_code?id=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-aluguel)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-aluguel&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=pm-grupo-va-de-bike_projeto-programacao-modular-unirio-aluguel)
